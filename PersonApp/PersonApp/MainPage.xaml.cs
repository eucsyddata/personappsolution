﻿using PersonApp.Model;
using PersonApp.Services;
using System;
using Xamarin.Forms;

namespace PersonApp
{
    public partial class MainPage : ContentPage
    {
        PersonDataService service;

        public MainPage()
        {
            InitializeComponent();
            service = new PersonDataService();

            pickerPersons.ItemsSource = service.GetAllPersons();
        }

        void Picker_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        void BtnShowAge_Clicked(object sender, EventArgs e)
        {

        }

        void BtnMakeOlder_Clicked(object sender, EventArgs e)
        {

        }

        void BtnEditPerson_Clicked(object sender, EventArgs e)
        {

        }

        void BtnAddPerson_Clicked(object sender, EventArgs e)
        {

        }
    }
}
