﻿using PersonApp.Model;
using System.Collections.Generic;
using System.Linq;

namespace PersonApp.DAL
{
    public class PersonRepository : IPersonRepository
    {
        static List<Person> persons;

        public List<Person> GetPersons()
        {
            if (persons == null)
                LoadPersons();
            return persons;
        }

        public Person GetPersonById(int id)
        {
            if (persons == null)
                LoadPersons();
            return persons.Where(c => c.Id == id).FirstOrDefault();
        }

        public void UpdatePerson(Person person)
        {
            Person personToUpdate = persons.Where(c => c.Id == person.Id).FirstOrDefault();
            personToUpdate = person;
        }

        public void DeletePerson(Person person)
        {
            persons.Remove(person);
        }

        private void LoadPersons()
        {
            persons = new List<Person>
            {
                new Person { Id = 1, Name = "Katja", Age = 27 },
                new Person { Id = 2, Name = "Christian", Age = 32 },
                new Person { Id = 3, Name = "Helle", Age = 54 }
            };
        }
    }
}
