﻿using PersonApp.Model;
using System.Collections.Generic;

namespace PersonApp.DAL
{
    public interface IPersonRepository
    {
        List<Person> GetPersons();
        Person GetPersonById(int id);
        void UpdatePerson(Person person);
        void DeletePerson(Person person);
    }
}
