﻿using PersonApp.Model;
using PersonApp.Services;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PersonApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditPage : ContentPage
    {
        public EditPage(Person selectedPerson, PersonDataService service)
        {
            InitializeComponent();
        }

        private void BtnSave_Clicked(object sender, EventArgs e)
        {

        }

        private void BtnDelete_Clicked(object sender, EventArgs e)
        {

        }

        private void BtnCancel_Clicked(object sender, EventArgs e)
        {

        }
    }
}