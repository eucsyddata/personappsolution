﻿using PersonApp.Model;
using System.Collections.Generic;

namespace PersonApp.Services
{
    public interface IPersonDataService
    {
        void DeletePerson(Person person);
        List<Person> GetAllPersons();
        Person GetPersonDetail(int personId);
        void UpdatePerson(Person person);
    }
}
