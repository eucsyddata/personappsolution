﻿using PersonApp.DAL;
using PersonApp.Model;
using System.Collections.Generic;

namespace PersonApp.Services
{
    public class PersonDataService : IPersonDataService
    {
        IPersonRepository repository = new PersonRepository();

        public List<Person> GetAllPersons()
        {
            return repository.GetPersons();
        }

        public Person GetPersonDetail(int personId)
        {
            return repository.GetPersonById(personId);
        }

        public void UpdatePerson(Person person)
        {
            repository.UpdatePerson(person);
        }

        public void DeletePerson(Person person)
        {
            repository.DeletePerson(person);
        }
    }
}
